FROM python:3.8.2-alpine
WORKDIR /data
ENTRYPOINT [ "/usr/local/bin/youtube-dl" ]

ARG FFMPEG_ZIP_URL 
RUN apk add --update --no-cache --virtual .build-deps curl unzip && \
    echo curl -sSLo ffmpeg.zip "${FFMPEG_ZIP_URL}" && \
    curl -sSLo ffmpeg.zip "${FFMPEG_ZIP_URL}" && \
    unzip ffmpeg.zip ffmpeg && \
    install -D -o root -g root -m 0755 ffmpeg /usr/local/bin/ffmpeg && \
    rm -f ffmpeg* && \
    apk del .build-deps

# install latest always
RUN python3 -m pip --no-cache-dir install --upgrade youtube-dl
