## YouTube-DL  
ffmpeg binaries and youtube-dl in Python3 alpine image.  
You can simply pull videos from youtube without installing youtube-dl locally.  
By default, working directory is */data*, so you can mount output directory here.  
  
### Examples  
#### Extract audio  
```shell  
docker run --rm -it -v "$(pwd):/data" yuriyvlasov/youtube-dl -x --audio-format mp3 --audio-quality 320k 'https://<youtube-url>'  
```  
#### List available formats  
```shell  
docker run --rm -it -v "$(pwd):/data" yuriyvlasov/youtube-dl -F 'https://<youtube-url>'  
```  
#### Download video  
```shell  
docker run --rm -it -v "$(pwd):/data" yuriyvlasov/youtube-dl -f 95 'https://<youtube-url>'  
```  
  